include:
  - project: "${CI_PROJECT_NAMESPACE}/pipelines-as-code"
    file:
      - '/.gitlab/trigger.yml'
      - '/.gitlab/containers.yml'
      - '/.gitlab/rules.yml'
  - project: "${CI_PROJECT_NAMESPACE}/pipelines-as-code-config"
    file: '/config.yml'

# looks funny to have test before build, but in this case
# 'build' is building a container image, not the code
stages:
  - lint
  - test
  - build
  - deploy
  - trigger

# Template with the common config for the Python jobs
.python:
  image: registry.access.redhat.com/ubi8/python-39:1-57
  before_script:
    - pip install --upgrade pip==21.2.4
    - pip install pylint -r requirements.txt -r test_requirements.txt

lint-containerfile:
  image: ghcr.io/hadolint/hadolint:latest-debian
  stage: lint
  script:
    - hadolint Containerfile
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - Containerfile
        - .gitlab-ci.yml
      when: always
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: never

python-check:
  extends: .python
  stage: lint
  script:
    - find . -name '*.py' -exec pylint {} +
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - "src/**/*.py"
        - "tests/**/*.py"
      allow_failure: true
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: never

python-test:
  extends: .python
  stage: test
  script: pytest  # config handled by pytest.ini file in project source
  coverage: '/TOTAL.*\s([.\d]+)%/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
      junit: junit.xml
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - "src/**/*.py"
        - "tests/**/*.py"
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: always

update-koji-cache:
  image: "$CONTAINER_REGISTRY/$CONTAINER_REGISTRY_NAMESPACE/create-yum-repo:${CREATE_YUM_REPO_TAG}"
  variables:
    LOCKFILE_DEST: "src/create_yum_repo/${OS_PREFIX}${OS_VERSION}-image-manifest.lock.json"
    NEW_CACHE_PATH: "src/create_yum_repo/new_koji_cache.json"
    OLD_CACHE_PATH: "src/create_yum_repo/koji_cache.json"
    CLONED_CYR_NAME: "c-y-r"
    LOCKFILE_RAW_FORMAT_URL: "https://gitlab.com/$LOCKFILE_PROJECT/-/raw/main/$LOCKFILE_PATH"
    BRANCH_NAME: "koji-cache-update"
  before_script:
    - dnf -y install git 
  script:
    - |-
      set -x

      git clone "$URL_USING_TOKEN" $CLONED_CYR_NAME
      cd $CLONED_CYR_NAME

      git checkout -b $BRANCH_NAME

      #create a new file that will replace the old koji_cache.json 
      touch "$NEW_CACHE_PATH"

      #lockfile is added to the cloned repo
      curl $LOCKFILE_RAW_FORMAT_URL -H "Accept: application/json" -o $LOCKFILE_DEST

      make-koji-cache
      
      #checking that the new koji_cache file isn't empty.
      #If it is - something went wrong, exist without creating an MR
      if [[ -s "$NEW_CACHE_PATH" ]] ; then  
        #removing the lockfile, old cache and adding the new cache file
        #before comparing to remote branch if exists
        rm $LOCKFILE_DEST
        rm $OLD_CACHE_PATH
        mv $NEW_CACHE_PATH $OLD_CACHE_PATH

        git add .

        BRANCH_EXISTS=$(git ls-remote --heads $URL_USING_TOKEN $BRANCH_NAME | wc -l)
      
        #branch koji-cache-update exists on remote.
        #Checking for diffs in the koji_cache.json file
        if [[ "$BRANCH_EXISTS" == 1 ]]; then
          DIFF_REMOTE_KOJI_BRANCH=$(git diff origin/$BRANCH_NAME $OLD_CACHE_PATH | wc -l)
          LOG="Nothing to commit, remote branch \"$BRANCH_NAME\" is up to date. 
            \nNOTE - \"$BRANCH_NAME\" branch should have been deleted if the cache was updated. 
            \nPlease check if an MR from that branch wasn't merged, and make sure it is merged"

        #branch does not exist. Checking diff with main to know if MR needs to be created  
        else
          DIFF_REMOTE_MAIN_BRANCH=$(git diff origin/main $OLD_CACHE_PATH | wc -l)
          LOG="Nothing to commit, the koji-cache file found in main is up to date"
        fi

        if [[ "$DIFF_REMOTE_KOJI_BRANCH" == 0 || "$DIFF_REMOTE_MAIN_BRANCH" == 0 ]]; then
          echo -e $LOG
          exit 0
        fi

        git config --global user.email "cache-bot@koji.com"
        git config --global user.name "Cache-bot"

        DATE=$(date)
        git commit -m "koji_cache.json: update for $DATE"

        #have gotten here if branch exists in remote, and there is something to commit
        #this option is just in case - source branch needs to be deleted
        git push -uf origin $BRANCH_NAME -o merge_request.create \
          -o merge_request.remove_source_branch \
          -o merge_request.title="koji_cache.json: update for $DATE" \
          -o merge_request.remove_source_branch \
          -o merge_request.merge_when_pipeline_succeeds
      else
        echo -e "Something went wrong, the new cache file was created is empty.
        \nYou may want to check the structure of the lockfile"
        exit 0
      fi        
  rules:
    - !reference [.run_on_schedule]

trigger-pipeline:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      variables:
        CREATE_YUM_REPO_TAG: $CI_COMMIT_REF_SLUG
    - !reference [.trigger-template, rules]

