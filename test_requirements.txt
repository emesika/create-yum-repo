-i https://pypi.org/simple
pytest==7.1.2
pytest-asyncio==0.18.3
pytest-cov==3.0.0
respx==0.19.2
