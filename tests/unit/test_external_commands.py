"""Tests for create_yum_repo.external_commands"""
import subprocess
from pathlib import Path
from unittest.mock import patch

import pytest

from create_yum_repo.external_commands import (
    dnf_makecache,
    create_repo,
    create_index_html,
    curl,
)


@patch("create_yum_repo.external_commands.subprocess.run")
def test_create_repo(mock_subprocess_run):
    """Test create_repo"""
    command = "createrepo_c /tmp/repodir"
    expected = command.split()
    create_repo(Path("/tmp/repodir"))
    mock_subprocess_run.assert_called_once_with(
        expected, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
    )


@patch("create_yum_repo.external_commands.subprocess.run")
def test_create_index_html(mock_subprocess_run):
    """Test create_index_html"""
    command = "tree -H . /tmp/repodir -o /tmp/repodir/index.html"
    expected = command.split()
    create_index_html(Path("/tmp/repodir"))
    mock_subprocess_run.assert_called_once_with(
        expected, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
    )


@pytest.mark.parametrize(
    "repos,conf_file,verbose,command",
    [
        ((), None, False, "dnf makecache"),
        (
            (),
            Path("/var/lib/repos/dnf.conf"),
            True,
            "dnf -v -c /var/lib/repos/dnf.conf makecache",
        ),
        (
            ("auto",),
            Path("/var/lib/repos/dnf.conf"),
            False,
            "dnf -c /var/lib/repos/dnf.conf --repo=auto makecache",
        ),
    ],
)
@patch("create_yum_repo.external_commands.subprocess.run")
def test_dnf_makecache(mock_subprocess_run, repos, conf_file, verbose, command):
    """Test dnf_makecache"""
    expected = command.split(" ")
    dnf_makecache(*repos, conf_file=conf_file, verbose=verbose)
    mock_subprocess_run.assert_called_once_with(
        expected, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
    )


@patch("create_yum_repo.external_commands.subprocess.run")
def test_curl(mock_subprocess_run):
    """Test curl"""
    command = "curl --fail http://fake.url/some-path"
    expected = command.split()
    curl("http://fake.url/some-path")
    mock_subprocess_run.assert_called_once_with(
        expected, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
    )
