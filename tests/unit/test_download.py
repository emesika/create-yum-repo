"""Tests for create_yum_repo.download"""
import hashlib
import json
from itertools import repeat
from pathlib import Path
from typing import BinaryIO
from unittest.mock import patch, MagicMock

import pytest
import respx

from create_yum_repo.download import (
    _download_file,
    _try_index,
    _try_koji,
    DEFAULT_KOJI_INSTANCES,
    _load_koji_cache,
    _save_koji_cache,
    _download_package,
    _find_koji_package_name,
    SUPPORTED_ARCHES,
    download_packages,
    DownloadError,
)

UNIT_TEST_DIRECTORY = Path(__file__).parent
RESOURCES = UNIT_TEST_DIRECTORY.parent / "resources"


@pytest.mark.asyncio
async def test_download_package_tries_all_sources_when_one_function_raises(
    mock_nvra_lookup, tmp_path, async_client, mock_load_koji_cache
):
    """Test for _download_package to try all sources even when one function raises"""
    package = "glibc-2.34-39.el9"
    arch = "x86_64"

    mock_koji_cache = mock_load_koji_cache()

    with patch(
        "create_yum_repo.download._try_index", create=True
    ) as mock_try_index, patch(
        "create_yum_repo.download._try_koji", create=True
    ) as mock_try_koji:
        mock_try_koji.__name__ = "_try_koji"
        mock_try_koji.return_value = True

        mock_try_index.__name__ = "_try_index"
        mock_try_index.side_effect = RuntimeError("This is a testing side effect.")

        result = await _download_package(
            package,
            arch,
            tmp_path,
            mock_nvra_lookup,
            DEFAULT_KOJI_INSTANCES,
            mock_koji_cache,
            async_client,
        )

    # make sure we tried the package lookup for both `arch` and "noarch"
    assert mock_try_index.call_count == 2

    mock_try_koji.assert_called_once()

    assert result == (tmp_path / f"{package}.{arch}.rpm")


@pytest.mark.asyncio
async def test_download_package_tries_all_sources_and_fails(
    mock_nvra_lookup, tmp_path, async_client, mock_load_koji_cache
):
    """Test for _download_package to fail"""
    package = "glibc-2.34-39.el9"
    arch = "x86_64"

    mock_koji_cache = mock_load_koji_cache()

    with patch(
        "create_yum_repo.download._try_index", create=True
    ) as mock_try_index, patch(
        "create_yum_repo.download._try_koji", create=True
    ) as mock_try_koji:
        mock_try_index.__name__ = "_try_index"
        mock_try_index.return_value = False

        mock_try_koji.__name__ = "_try_koji"
        mock_try_koji.return_value = False

        with pytest.raises(DownloadError):
            await _download_package(
                package,
                arch,
                tmp_path,
                mock_nvra_lookup,
                DEFAULT_KOJI_INSTANCES,
                mock_koji_cache,
                async_client,
            )

    # make sure we tried with both `arch` and "noarch"
    assert mock_try_index.call_count == 2
    assert mock_try_koji.call_count == 2


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "package,expected",
    [("glibc-devel-2.34-39.el9", False), ("glibc-2.34-39.el9", True)],
)
async def test_try_index(package, expected, tmp_path, mock_nvra_lookup, async_client):
    """Test for _try_index to succeed"""
    arch = "x86_64"
    output_path = tmp_path / "test.rpm"

    with patch("create_yum_repo.download._download_file") as mock_download:
        result = await _try_index(
            package=package,
            arch=arch,
            output_path=output_path,
            nvras=mock_nvra_lookup,
            client=async_client,
        )
        assert result is expected

    if result:
        mock_download.assert_called_once_with(
            async_client,
            ("https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/"
             "BaseOS/x86_64/os/Packages/glibc-2.34-39.el9.x86_64.rpm"),
            output_path,
        )
    else:
        mock_download.assert_not_called()


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "package,arch,output_path,nvras,client",
    [
        (None, "x86_64", Path("/tmp/test.txt"), {}, respx.mock()),
        ("glibc-2.34-39.el9", None, Path("/tmp/test.txt"), {}, respx.mock()),
        ("glibc-2.34-39.el9", "x86_64", None, {}, respx.mock()),
        ("glibc-2.34-39.el9", "x86_64", Path("/tmp/test.txt"), None, respx.mock()),
        ("glibc-2.34-39.el9", "x86_64", Path("/tmp/test.txt"), {}, None),
    ],
)
async def test_try_index_raises_on_missing_argument(
    package, arch, output_path, nvras, client
):
    """Test for _try_index to raise exception on missing argument"""
    kwargs = {
        "package": package,
        "arch": arch,
        "output_path": output_path,
        "nvras": nvras,
        "client": client,
    }
    with pytest.raises(ValueError):
        await _try_index(**kwargs)


# pylint: disable=too-many-arguments
@pytest.mark.asyncio
@pytest.mark.parametrize(
    "package,arch,output_path,koji_mirrors,koji_cache,client",
    [
        (None, "x86_64", Path("/tmp/test.txt"), {}, {"glibc": "glibc"}, respx.mock()),
        (
            "glibc-2.34-39.el9",
            None,
            Path("/tmp/test.txt"),
            {},
            {"glibc": "glibc"},
            respx.mock(),
        ),
        ("glibc-2.34-39.el9", "x86_64", None, {}, {"glibc": "glibc"}, respx.mock()),
        (
            "glibc-2.34-39.el9",
            "x86_64",
            Path("/tmp/test.txt"),
            None,
            {"glibc": "glibc"},
            respx.mock(),
        ),
        ("glibc-2.34-39.el9", "x86_64", Path("/tmp/test.txt"), {}, None, respx.mock()),
        (
            "glibc-2.34-39.el9",
            "x86_64",
            Path("/tmp/test.txt"),
            {},
            {"glibc": "glibc"},
            None,
        ),
    ],
)
async def test_try_koji_raises_on_missing_argument(
    package, arch, output_path, koji_mirrors, koji_cache, client
):
    """Test for _try_koji to raise on missing argument"""
    kwargs = {
        "package": package,
        "arch": arch,
        "output_path": output_path,
        "koji_mirrors": koji_mirrors,
        "koji_cache": koji_cache,
        "client": client,
    }
    with pytest.raises(ValueError):
        await _try_koji(**kwargs)


@pytest.mark.asyncio
async def test_try_koji_does_not_download_when_no_koji_name_is_found(
    mock_koji_cache,
    tmp_path,
    async_client,
):
    """Test for _try_koji to fail when no koji package can be found"""
    arch = "x86_64"
    output_path = tmp_path / "test.rpm"
    package = "some-random-package-0.1.0-el9"

    with patch("create_yum_repo.download._download_file") as mock_download, patch(
        "create_yum_repo.download._find_koji_package_name"
    ) as mock_find_koji_name:
        mock_find_koji_name.return_value = None

        result = await _try_koji(
            package=package,
            arch=arch,
            output_path=output_path,
            koji_mirrors=[
                ("https://kojihub.stream.centos.org/kojihub/|"
                 "https://kojihub.stream.centos.org/kojifiles/packages/")
            ],
            koji_cache=mock_koji_cache,
            client=async_client,
        )

        assert not result

    mock_find_koji_name.assert_called()
    mock_download.assert_not_called()


@pytest.mark.parametrize(
    "path,exists",
    [
        (RESOURCES / "fake_koji_cache.json", True),
        (RESOURCES / "doesnt_exist.json", False),
    ],
)
def test_load_koji_cache(path, exists):
    """Test for _load_koji_cache"""
    expected = {"glibc": "glibc", "glibc-devel": "glibc"} if exists else {}
    actual = _load_koji_cache(path)
    assert actual == expected


@pytest.mark.parametrize(
    "cache", [{}, None, {"glibc": "glibc", "glibc-devel": "glibc"}]
)
def test_save_koji_cache(cache, tmp_path):
    """Test _save_koji_cache"""
    actual = tmp_path / "test_koji_cache.json"

    if cache:
        expected = RESOURCES / "fake_koji_cache.json"
    else:
        expected = RESOURCES / "fake_koji_cache_empty.json"

    _save_koji_cache(cache, actual)
    assert_files(expected, actual)


@pytest.mark.parametrize(
    "package,rpm_response,build_response,expected",
    [
        ("glibc-devel-2.34-39.el9", "resources", "resources", "glibc"),
        ("glibc-2.34-39.el9", "resources", None, None),
        ("wont-find-an-rpm", None, None, None),
        (  # exercise a corner case
            "nspr-devel-4.34.0-7.el9",
            "resources",
            {"name": "nspr"},
            "nss",
        ),
    ],
)
def test_find_koji_package_name(package, rpm_response, build_response, expected):
    """Test _find_koji_package_name"""
    arch = "x86_64"
    koji = MagicMock(getRPM=MagicMock(), getBuild=MagicMock())

    if rpm_response == "resources":
        with (RESOURCES / "getRPM.json").open("r") as get_rpm_json:
            koji.getRPM.return_value = json.load(get_rpm_json)
    else:
        koji.getRPM.return_value = rpm_response

    if build_response == "resources":
        with (RESOURCES / "getBuild.json").open("r") as get_build_json:
            koji.getBuild.return_value = json.load(get_build_json)
    else:
        koji.getBuild.return_value = build_response

    actual = _find_koji_package_name(package, arch, koji)

    assert actual == expected


@pytest.mark.asyncio
async def test_download_file(async_client, respx_mock, repomd_xml_path, tmp_path):
    """Test _download_file"""
    url = "http://example.com"
    route = respx_mock.get(url)
    output_path = tmp_path / "fake.xml"

    with repomd_xml_path.open("rb") as content:
        route.respond(content=content.read())
        await _download_file(async_client, url, output_path)

    assert_files(repomd_xml_path, output_path)


def assert_files(expected: Path, actual: Path):
    """Assert files match"""
    with expected.open("rb") as expected_f, actual.open("rb") as actual_f:
        assert _hash(expected_f) == _hash(actual_f)


def _hash(file: BinaryIO) -> str:
    """Get hex digest of file"""
    digester = hashlib.sha256()
    while chunk := file.read(4096):
        digester.update(chunk)

    return digester.hexdigest()


@pytest.mark.asyncio
async def test_download_packages(
    mock_nvra_lookup, async_client, tmp_path
):
    """Test download_packages"""
    lockfile = RESOURCES / "fake_lockfile.lock.json"

    # create one existing repo structure to exercise a particular branch
    (tmp_path / "cs9/x86_64/os/Packages").mkdir(parents=True)
    (tmp_path / "cs9/source/Packages").mkdir(parents=True)

    with lockfile.open() as lockfile_f:
        lock = json.load(lockfile_f)

    # duplicate each common package once per supported arch
    packages = lock["cs9"]["common"] * len(SUPPORTED_ARCHES)
    # then add the arch-specific packages
    for key in SUPPORTED_ARCHES:
        packages.extend(lock["cs9"]["arch"][key])

    packages.extend(lock["cs9"]["src"])

    with patch("create_yum_repo.download.async_client") as mock_http_factory, patch(
        "create_yum_repo.download._download_package"
    ) as mock_download_package:
        mock_http_factory.return_value = async_client
        await download_packages(
            "cs9",
            lockfile,
            mock_nvra_lookup,
            DEFAULT_KOJI_INSTANCES,
            tmp_path,
        )

    assert mock_download_package.call_count == len(packages)
    assert async_client.is_closed


@pytest.mark.asyncio
async def test_download_packages_raises_on_malformed_lockfile(
    mock_nvra_lookup, tmp_path
):
    """Test download_packages error case with bad lockfile"""
    lockfile = RESOURCES / "bad_lockfile.lock.json"
    with pytest.raises(KeyError):
        await download_packages(
            "cs9",
            lockfile,
            mock_nvra_lookup,
            DEFAULT_KOJI_INSTANCES,
            tmp_path,
        )


@pytest.mark.asyncio
async def test_try_koji_name_in_cache_koji_has_rpm(
    mock_koji_cache, tmp_path, async_client
):
    """Test _try_koji case where koji_cache is valid and rpm is available"""
    package = "glibc-2.34-39.el9"
    arch = "x86_64"
    output_path = tmp_path / f"{package}.rpm"

    n_mirrors = 5
    koji_mirrors = repeat(
        "https://127.0.0.1/kojihub/|https://127.0.0.1/kojifiles/packages/", n_mirrors
    )

    with patch("create_yum_repo.download._download_file") as mock_download_file, patch(
        "create_yum_repo.download._find_koji_package_name"
    ) as mock_find_koji_name:
        # make sure download_package raises on all but one mirror
        the_one_with_the_rpm = n_mirrors // 2
        download_results = [RuntimeError] * n_mirrors
        download_results[the_one_with_the_rpm] = None

        mock_download_file.side_effect = download_results

        result = await _try_koji(
            package=package,
            arch=arch,
            output_path=output_path,
            koji_mirrors=koji_mirrors,
            koji_cache=mock_koji_cache,
            client=async_client,
        )

        assert (
            result
        ), "_try_koji failed to download the test package (unexpected failure)"
        mock_find_koji_name.assert_not_called()
        assert mock_download_file.call_count == the_one_with_the_rpm + 1


@pytest.mark.asyncio
async def test_try_koji_name_in_cache_but_koji_doesnt_have_rpm(
    mock_koji_cache, tmp_path, async_client
):
    """Test _try_koji case where koji_cache is valid and rpm is unavailable"""
    package = "glibc-2.34-39.el9"
    arch = "x86_64"
    output_path = tmp_path / f"{package}.rpm"

    n_mirrors = 5
    koji_mirrors = repeat(
        "https://127.0.0.1/kojihub/|https://127.0.0.1/kojifiles/packages/", n_mirrors
    )

    with patch("create_yum_repo.download._download_file") as mock_download_file, patch(
        "create_yum_repo.download._find_koji_package_name"
    ) as mock_find_koji_name:
        # make sure download_package raises on all but one mirror
        download_results = [RuntimeError] * n_mirrors
        mock_download_file.side_effect = download_results

        result = await _try_koji(
            package=package,
            arch=arch,
            output_path=output_path,
            koji_mirrors=koji_mirrors,
            koji_cache=mock_koji_cache,
            client=async_client,
        )

        assert (
            not result
        ), "_try_koji did not fail to download an unavailable test package (unexpected success)."
        mock_find_koji_name.assert_not_called()
        assert mock_download_file.call_count == n_mirrors


# pylint: disable=too-many-locals
@pytest.mark.asyncio
async def test_try_koji_name_not_in_cache_but_koji_knows_and_has_rpm(
    mock_koji_cache, tmp_path, async_client
):
    """Test _try_koji case where koji_cache is invalid but rpm is available"""
    # a package name not in the mock cache
    package = "glibc-devel-2.34-39.el9"
    arch = "x86_64"
    output_path = tmp_path / f"{package}.rpm"

    n_mirrors = 5
    koji_mirrors = repeat(
        "https://127.0.0.1/kojihub/|https://127.0.0.1/kojifiles/packages/", n_mirrors
    )

    with patch("create_yum_repo.download._download_file") as mock_download_file, patch(
        "create_yum_repo.download._find_koji_package_name"
    ) as mock_find_koji_name:
        # simulate that
        # one koji raises and some subsequent koji knows the name
        # if there is only one mirror then make sure it knows the name
        the_one_that_raises = max(0, (n_mirrors // 2) - 1)
        the_one_with_the_name = n_mirrors // 2

        koji_results = [None] * n_mirrors
        koji_results[the_one_that_raises] = RuntimeError
        koji_results[the_one_with_the_name] = "glibc"
        mock_find_koji_name.side_effect = koji_results

        # simulate that download_package raises on all but one mirror
        # worst case: make that one mirror be the last mirror
        # lastly, account for the fact that `the_one_with_the_name`
        # mirrors will be tried before _download_file is called the first
        # time
        download_calls = max(1, n_mirrors - the_one_with_the_name)
        the_one_with_the_rpm = download_calls - 1
        download_results = [RuntimeError] * download_calls
        download_results[the_one_with_the_rpm] = None

        mock_download_file.side_effect = download_results

        result = await _try_koji(
            package=package,
            arch=arch,
            output_path=output_path,
            koji_mirrors=koji_mirrors,
            koji_cache=mock_koji_cache,
            client=async_client,
        )

        assert (
            result
        ), "_try_koji failed to download the test package (unexpected failure)"
        assert (
            mock_find_koji_name.call_count == the_one_with_the_name + 1
        ), "_find_koji_name was not called the expected number of times"
        assert (
            mock_download_file.call_count == the_one_with_the_rpm + 1
        ), "_download_file was not called the expected number of times"


@pytest.mark.asyncio
async def test_try_koji_name_cannot_be_found(mock_koji_cache, tmp_path, async_client):
    """Test _try_koji case where koji_cache is invalid but rpm is unavailable"""
    package = "glibc-devel-2.34-39.el9"
    arch = "x86_64"
    output_path = tmp_path / f"{package}.rpm"

    n_mirrors = 5
    koji_mirrors = repeat(
        "https://127.0.0.1/kojihub/|https://127.0.0.1/kojifiles/packages/", n_mirrors
    )

    with patch("create_yum_repo.download._download_file") as mock_download_file, patch(
        "create_yum_repo.download._find_koji_package_name"
    ) as mock_find_koji_name:
        # simulate that
        # one koji raises and the rest of the kojis don't know the name
        the_one_that_raises = n_mirrors // 2
        koji_results = [None] * n_mirrors
        koji_results[the_one_that_raises] = RuntimeError
        mock_find_koji_name.side_effect = koji_results

        result = await _try_koji(
            package=package,
            arch=arch,
            output_path=output_path,
            koji_mirrors=koji_mirrors,
            koji_cache=mock_koji_cache,
            client=async_client,
        )

        assert (
            not result
        ), "_try_koji did not fail to download an unavailable test package (unexpected success)."
        assert (
            mock_find_koji_name.call_count == n_mirrors
        ), "_find_koji_name was not called as many times as expected"
        mock_download_file.assert_not_called()
