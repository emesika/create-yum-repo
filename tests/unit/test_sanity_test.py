"""Test create_yum_repo.sanity_test"""
import sys
import subprocess
from unittest.mock import patch, call

import pytest

from create_yum_repo import sanity_test
from create_yum_repo.sanity_test import (
    verify_repo,
    download_pkg_with_plus,
)


@pytest.mark.asyncio
async def test_verify_repo():
    """Test verify_repo"""
    with patch(
        "create_yum_repo.sanity_test.dnf_makecache"
    ) as mock_dnf_makecache, patch(
        "create_yum_repo.sanity_test.download_pkg_with_plus"
    ) as mock_download_pkg_with_plus:
        await verify_repo("file://fake.repo", "x86_64")

    mock_dnf_makecache.assert_called()
    mock_download_pkg_with_plus.assert_called()


def test_entrypoint():
    """Test run"""
    repo = "https://fake-repo"
    with patch.object(sys, "argv", ["repo-sanity-test", repo]), patch(
        "create_yum_repo.sanity_test.verify_repo"
    ) as mock_verify_repo:
        sanity_test.run()

    mock_verify_repo.assert_has_calls(
        [call(repo, arch) for arch in ("aarch64", "x86_64")]
    )


def test_download_pkg_with_plus():
    """Test download_pkg_with_plus"""
    command = "curl --fail http://fake.url/repos/cs9/aarch64/os/Packages/gcc-c++-11.3.1-2.1.el9.aarch64.rpm" #pylint: disable=line-too-long
    expected = command.split()
    with patch(
        "create_yum_repo.external_commands.subprocess.run"
    ) as mock_subprocess_run, patch(
        "create_yum_repo.sanity_test._find_pkg_name"
    ) as mock_find_pkg_name:
        mock_find_pkg_name.return_value = "gcc-c++-11.3.1-2.1.el9.aarch64.rpm"

        download_pkg_with_plus("http://fake.url/repos/cs9", "aarch64")

        mock_subprocess_run.assert_called_once_with(
            expected, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
        )
