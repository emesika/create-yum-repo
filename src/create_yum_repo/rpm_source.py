"""Class definition for RpmSource"""

class RpmSource:
    """Simple model class representing where an RPM can be downloaded.

    This class is intended for use with a priority queue, so the rich
    comparison methods are implemented specifically with that in mind.

    Parameters:
        uri (:py:obj:`str`): The URI of an RPM.
        priority (:py:obj:`int`, optional): This source's priority value.

    """

    def __init__(self, uri: str, priority: int = 0):
        self._uri = uri
        self._priority = priority

    def __str__(self) -> str:
        return f"RpmSource(priority={self.priority}, uri={self.uri})"

    def __repr__(self) -> str:
        return self.__str__()

    def __eq__(self, other: object) -> bool:
        self._comparison_type_check(other, "==")
        return self.priority == other.priority

    def __ne__(self, other: object) -> bool:
        self._comparison_type_check(other, "!=")
        return self.priority != other.priority

    def __lt__(self, other: object) -> bool:
        self._comparison_type_check(other, "<")
        return self.priority < other.priority

    def __le__(self, other: object) -> bool:
        self._comparison_type_check(other, "<=")
        return self.priority <= other.priority

    def __gt__(self, other: object) -> bool:
        self._comparison_type_check(other, ">")
        return self.priority > other.priority

    def __ge__(self, other: object) -> bool:
        self._comparison_type_check(other, ">=")
        return self.priority >= other.priority

    @property
    def uri(self) -> str:
        """The download URI represented by this RpmSource."""
        return self._uri

    @property
    def priority(self) -> int:
        """This source's priority value."""
        return self._priority

    def equals(self, other):
        """Perform a deep comparison for equality.

        In order to be useful in a PriorityQueue, this class implements
        all the rich comparison operators to compare solely on priority
        value. This means that code like `rpm_source1 == rpm_source2`
        does not capture whether two RpmSource objects have the same priority
        _and_ refer to the same URI.

        To get a comparison for equality based on all fields use this method
        instead of the `==` operator.
        """
        self._comparison_type_check(other, "equals")
        return self == other and self.uri == other.uri

    def _comparison_type_check(self, other: object, operator: str):
        if not isinstance(other, RpmSource):
            raise TypeError(
                f"Cannot compare {self.__class__.__name__} and {type(other)} with {operator}"
            )
