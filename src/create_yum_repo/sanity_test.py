#!/usr/bin/env python3

"""Entrypoint for yum repo sanity test program"""

import re
import asyncio
import logging
import logging.config
import tempfile

from argparse import ArgumentParser
from pathlib import Path
from urllib.parse import urljoin

import httpx
from jinja2 import Template

from .external_commands import dnf_makecache, curl


async def verify_repo(baseurl: str, arch: str):
    """Call dnf makecache to test the created repos are valid"""
    logging.info("Testing dnf repo at %s for %s", baseurl, arch)

    # Configure and call dnf using a custom dnf.conf to avoid contaminating the host system
    templates = Path(__file__).parent / "templates"
    dnfconf_template_file = templates / "dnf.conf.j2"
    repofile_template_file = templates / "yumrepo.j2"

    with tempfile.TemporaryDirectory() as tmpdirname:
        tmpdir = Path(tmpdirname)

        (tmpdir / "cache").mkdir(parents=True)
        (tmpdir / "logs").mkdir(parents=True)
        (tmpdir / "persist").mkdir(parents=True)
        (tmpdir / "repos").mkdir(parents=True)

        dnfconf_file = tmpdir / "dnf.conf"
        repofile = tmpdir / "repos" / "automotive.repo"

        dnfconf = Template(dnfconf_template_file.read_text()).render(tmpdir=tmpdirname)
        dnfconf_file.write_text(dnfconf)

        repoconf = Template(repofile_template_file.read_text()).render(
            arch=arch, baseurl=baseurl
        )
        repofile.write_text(repoconf)
        logging.info(repofile)

        dnf_makecache("auto", conf_file=tmpdir / "dnf.conf", verbose=True)
        download_pkg_with_plus(baseurl, arch)


def download_pkg_with_plus(baseurl: str, arch: str):
    """Download a package with the character '+' from the repo.

    osbuild uses curl to download the packages from the repo, at S3.
    But it fails with packages with the character +, depending on the URL
    used to access to the S3 bucket.
    This is a regresion test to avoid using the wrong URL and hit the + bug.
    """
    url_re = r"(.*\/)(.*$)"
    repo_url, distro = re.findall(url_re, baseurl)[0]
    pkg_name = _find_pkg_name(repo_url, arch)
    pkg_path = distro + "/" + arch + "/os/Packages/" + pkg_name
    pkg_url = urljoin(repo_url, pkg_path)

    curl(pkg_url)


def _find_pkg_name(repo_url: str, arch: str):
    index_url = urljoin(repo_url, "index.html")
    response = httpx.get(index_url)
    body = response.text

    pkg_name_re = r'<a href=".*?">(gcc-c.*.' + arch + ".rpm)</a>"
    pkg_name = re.findall(pkg_name_re, body)[0]
    return pkg_name


def arg_parser():
    """Construct ArgumentParser instance"""
    parser = ArgumentParser(
        description="Verifies YUM repo metadata are well-formed by calling dnf makecache."
    )
    parser.add_argument("repo", help="The URL of the YUM repo to be verified.")
    return parser


async def main():
    """Main function"""
    logging.info(":: test-yum-repo started ::")

    parser = arg_parser()
    args = parser.parse_args()

    supported_arches = ["aarch64", "x86_64"]

    logging.info("Testing repos at: %s", args.repo)

    tasks = [
        asyncio.create_task(verify_repo(args.repo, arch)) for arch in supported_arches
    ]
    await asyncio.gather(*tasks)
    logging.info("Success!")


def run():
    """Script entrypoint"""
    asyncio.get_event_loop().run_until_complete(main())


if __name__ == "__main__":
    run()
