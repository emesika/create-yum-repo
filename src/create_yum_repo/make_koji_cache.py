#!/usr/bin/env python3
"""Script for generating koji_cache.json"""

import json
import os
import sys

import koji

# Developer Note: About GitLab CI Usage of this script
# This script is called in the GitLab CI of create-yum-repo via a scheduled
# job to update the koji_cache.json file.
# Please note that in order to test changes that are done in this file in the
# scheduled job, you have to first update the create-yum-repo container before
# testing the scheduled job. This is because the version of that file that is
# executed is always the version from the container.

koji_session = koji.ClientSession("https://kojihub.stream.centos.org/kojihub")


def _find_koji_package_name_from_nvra(nvra):
    # Try finding the koji build up to 3 times to handle instabilities
    for _ in range(0, 3):
        find_build = koji_session.getRPM(nvra)
        if find_build:
            break
    else:
        # Didn't find the build so return None
        return None

    # Try finding the koji package up to 3 times to handle instabilities
    for _ in range(0, 3):
        find_package = koji_session.getBuild(find_build["build_id"])
        if find_package:
            break
    else:
        # Didn't find the package so return None
        return None

    package_name = find_package["package_name"]

    if package_name == "nspr":
        package_name = "nss"

    return package_name


def _find_koji_package_name(package, arch):
    for pkg_arch in [arch, "noarch"]:
        nvra = f"{package}.{pkg_arch}"
        package_name = _find_koji_package_name_from_nvra(nvra)
        return package_name


def _save_koji_cache(new_cache_name: str, data: dict):
    koji_cache_data = json.dumps(data, indent=2) + os.linesep

    with open(new_cache_name, "w", encoding="UTF-8") as koji_cache_f:
        koji_cache_f.write(koji_cache_data)


def main():
    """Main function"""
    # Current path is relative to whom called this program.
    # We want to make it clear that all files we are working with will be relative to the path
    # of which make_koji_cache.json is found at
    new_cache_path = os.getenv("NEW_CACHE_PATH")
    lockfile_path = os.getenv("LOCKFILE_DEST")

    with open(lockfile_path, "r", encoding="UTF-8") as lockfile_f:
        lockfile_packages = json.load(lockfile_f)

    distros = lockfile_packages.keys()
    data = {}

    for distro in distros:
        for arch in ["aarch64", "x86_64"]:
            for package in (
                lockfile_packages[distro]["common"]
                + lockfile_packages[distro]["arch"][arch]
            ):
                name, _, _ = package.rsplit("-", 2)
                package_name = _find_koji_package_name(package, arch)

                if not package_name:
                    continue

                data[name] = package_name
                _save_koji_cache(new_cache_path, data)

    return 0


if __name__ == "__main__":
    sys.exit(main())
