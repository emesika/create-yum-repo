"""Download functions for create-yum-repo"""

import asyncio
import json
import logging
import os
import shutil
from asyncio import PriorityQueue
from pathlib import Path
from typing import Any, Optional, Mapping, MutableMapping, Collection
from urllib.parse import urljoin

import httpx
import koji
from aiofile import AIOFile
from httpx import Timeout, HTTPStatusError, codes

MIRROR_URL_PREFIX = "http://mirror.stream.centos.org/9-stream"
COMPOSE_URL_PREFIX = "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose"
CENTOS_REPOS = ["BaseOS", "AppStream", "CRB"]
SUPPORTED_ARCHES = ["aarch64", "x86_64"]
DEFAULT_YUM_REPOS = [
    f"{MIRROR_URL_PREFIX}/{{centos_repo}}/{{arch}}/os/",
    f"{MIRROR_URL_PREFIX}/{{centos_repo}}/source/tree/",
    f"{COMPOSE_URL_PREFIX}/{{centos_repo}}/{{arch}}/os/",
    f"{COMPOSE_URL_PREFIX}/{{centos_repo}}/source/tree/",
    "https://buildlogs.centos.org/9-stream/automotive/{arch}/packages-main/",
    "https://buildlogs.centos.org/9-stream/autosd/{arch}/packages-main/",
]
DEFAULT_KOJI_INSTANCES = [
    ("https://kojihub.stream.centos.org/kojihub/|"
     "https://kojihub.stream.centos.org/kojifiles/packages/"),
    ("https://cbs.centos.org/kojihub/|"
     "https://cbs.centos.org/kojifiles/packages/"),
]


class DownloadError(Exception):
    """Custom DownloadError exception"""


def async_client(
    workers: int = 8, retries: int = 3, timeout: float = 5.0
) -> httpx.AsyncClient:
    """Create a httpx.AsyncClient"""
    limits = httpx.Limits(
        max_keepalive_connections=workers, max_connections=workers * 2
    )
    transport = httpx.AsyncHTTPTransport(retries=retries, limits=limits)
    return httpx.AsyncClient(
        transport=transport, timeout=Timeout(timeout), follow_redirects=True
    )


async def download_packages(
    base: str,
    lockfile: Path,
    nvras: Mapping[str, PriorityQueue],
    koji_mirrors: Collection[str],
    output_dir: Path,
):
    """Download the set of packages in `lockfile` using `nvras` as a download URL lookup.

    If a package in the lockfile cannot be found in the package lookup, a list of Koji
    mirrors is used as a fallback.

    Parameters:
        base: The base distribution for the packages being downloaded.
        lockfile: The path to the lockfile.
        nvras: A mapping from package NVRA to its download URLs in descending priority order.
        koji_mirrors: A collection of Koji mirror URLs that will be used in the event
            a package download URL can't be found in the NVRA lookup.
        output_dir: The parent directory where the YUM repo structure will be created and
            downloaded RPMs will be written to.
    """
    #pylint: disable=too-many-locals

    # Create clean output directory structure
    output_dir.mkdir(parents=True, exist_ok=True)

    with lockfile.open() as lockfile_f:
        lock = json.load(lockfile_f)

    if base not in lock:
        raise KeyError(
            "Malformed lockfile: top level key did not match 'cs9' or 'rhel9'."
        )

    cache_path = Path(__file__).parent / "koji_cache.json"
    koji_cache = _load_koji_cache(cache_path)

    # Increase timeout from default to 60.0s to avoid network timeout issues
    # being seen when interacting with centos mirrors
    client = async_client(timeout=60.0)
    tasks = []
    common = lock[base]["common"]

    # Handle normal packages
    for arch in SUPPORTED_ARCHES:
        repo_path = output_dir / base / arch / "os" / "Packages"

        if repo_path.exists():
            logging.info("Removing existing repo structure at %s", repo_path)
            shutil.rmtree(str(repo_path))

        logging.info("Creating repo structure at %s", repo_path)
        repo_path.mkdir(parents=True)

        packages = common + lock[base]["arch"][arch]
        for package in packages:
            future = asyncio.create_task(
                _download_package(
                    package, arch, repo_path, nvras, koji_mirrors, koji_cache, client
                )
            )
            tasks.append(future)

    # Handle source packages
    source_repo_path = output_dir / base / "source" / "Packages"

    if source_repo_path.exists():
        logging.info("Removing existing repo structure at %s", source_repo_path)
        shutil.rmtree(str(source_repo_path))

    logging.info("Creating source repo structure at %s", source_repo_path)
    source_repo_path.mkdir(parents=True)

    # Build source repository
    if "src" in lock[base]:
        for src_package in lock[base]["src"]:
            # remove .src from package name (until proper nvra support added to lockfile)
            src_package = src_package[:-4]
            future = asyncio.create_task(
                _download_package(
                    src_package,
                    "src",
                    source_repo_path,
                    nvras,
                    koji_mirrors,
                    koji_cache,
                    client,
                )
            )
            tasks.append(future)

    logging.info("Beginning all package downloads (%d tasks in queue)...", len(tasks))
    # asyncio.gather with `return_exceptions=True` returns the exceptions in the results
    results = await asyncio.gather(*tasks, return_exceptions=True)

    failed_downloads = []
    for result in results:
        if isinstance(result, Exception):
            # The exceptions coming out of asyncio.gather have lackluster traceback logs
            # so we do not bother printing them
            failed_downloads.append(str(result))

    await client.aclose()
    _save_koji_cache(koji_cache, cache_path)

    if failed_downloads:
        failed_packages = " * " + "\n * ".join(failed_downloads)
        logging.error(
            "Failed to download the following packages from any download option! "
            "This can be caused due to network failures or the package is not in "
            "the configured repos/koji instances. "
            "The package may also have expired from the sources.\n%s",
            failed_packages,
        )
        raise DownloadError(failed_packages)


#pylint: disable=too-many-arguments,broad-except
async def _download_package(
    package: str,
    package_arch: str,
    output_dir: Path,
    nvras: Mapping[str, PriorityQueue],
    koji_mirrors: Collection[str],
    koji_cache: MutableMapping[str, str],
    client: httpx.AsyncClient,
) -> Path:

    kwargs = {
        "package": package,
        "nvras": nvras,
        "koji_mirrors": koji_mirrors,
        "koji_cache": koji_cache,
        "client": client,
    }

    # in order of preference, try these sources for a package
    for func in [_try_index, _try_koji]:
        # first try it by the arch we know, then fallback to noarch
        for arch in [package_arch, "noarch"]:
            kwargs["arch"] = arch

            rpm_name = f"{package}.{arch}.rpm"
            output_file = output_dir / rpm_name
            kwargs["output_path"] = output_file

            try:
                if await func(**kwargs):
                    return output_file
            except Exception:
                logging.warning(
                    "Failed to find %s.%s. Trying next arch...", package, arch
                )
                continue
        logging.warning(
            "Failed to find %s with %s. Trying next option...", package, func.__name__
        )

    raise DownloadError(f"{package}[{package_arch}]")


async def _try_index(
    *,
    package: str = None,
    arch: str = None,
    output_path: Path = None,
    nvras: Mapping[str, PriorityQueue] = None,
    client: httpx.AsyncClient = None,
    **_,
) -> bool:
    if any((arg is None for arg in (package, arch, output_path, nvras, client))):
        raise ValueError(
            "Unable to try compose mirrors due to unfulfilled function argument."
        )
    if (nvra := f"{package}-{arch}") not in nvras:
        return False
    source = nvras[nvra].get_nowait()
    logging.info("Found %s in package lookup! Downloading %s...", nvra, source)
    await _download_file(client, source.uri, output_path)
    return True


#pylint: disable=too-many-locals
async def _try_koji(
    *,
    package: str = None,
    arch: str = None,
    output_path: Path = None,
    koji_mirrors: list[str] = None,
    koji_cache: MutableMapping[str, str] = None,
    client: httpx.AsyncClient = None,
    **_,
):
    if any(
        (
            arg is None
            for arg in (package, arch, output_path, koji_mirrors, koji_cache, client)
        )
    ):
        raise ValueError(
            "Failed to try Koji mirrors due to unfulfilled function argument"
        )

    for mirror in koji_mirrors:
        api_url, packages_url = mirror.split("|")
        # Have to search koji/brew for the package name
        # eg. libgcc comes from gcc
        session = koji.ClientSession(api_url, opts={"no_ssl_verify": True})
        # Find build of RPM
        logging.info("Trying Koji/Brew (%s) for %s.%s", mirror, package, arch)

        # break apart the package name for all the URL path
        # components needed for Koji
        name, version, release = package.rsplit("-", 2)

        # see if we've ever come across this package before, and
        # can save the API call to find out where it comes from
        koji_build_name = koji_cache.get(name)
        if not koji_build_name:
            logging.info(
                "Could not find package name for %s in the cache. "
                "Trying Koji API to determine the proper package name...",
                package,
            )
            try:
                koji_build_name = _find_koji_package_name(package, arch, session)
            except Exception:
                koji_build_name = None

        if koji_build_name:
            # found it, save in the cache and continue with download
            logging.info("Found package name %s for NVR %s", koji_build_name, package)
            koji_cache[name] = koji_build_name

            url = urljoin(
                packages_url,
                f"{koji_build_name}/{version}/{release}/{arch}/{output_path.name}",
            )

            try:
                await _download_file(client, url, output_path)
                logging.info("Downloaded %s.%s from Koji %s", package, arch, mirror)
                return True
            except HTTPStatusError as hse:
                if hse.response.status_code == codes.NOT_FOUND:
                    logging.info(
                        "Could not find package %s.%s at Koji %s (Status Code 404)",
                        package,
                        arch,
                        mirror,
                    )
                else:
                    logging.exception(
                        "Unexpected error occurred with the request to download %s.%s from %s",
                        package,
                        arch,
                        mirror,
                    )
            except Exception:
                # ignore exception and try next mirror
                logging.exception(
                    "Unhandled error occurred during download of %s.%s from %s",
                    package,
                    arch,
                    mirror,
                )
        else:
            logging.warning(
                "Unable to find package information for %s.%s from Koji at %s",
                package,
                arch,
                api_url,
            )

    # tried all mirrors, return False
    return False


def _load_koji_cache(path: Path) -> MutableMapping[Any, Any]:
    if not path.exists():
        return {}

    with path.open("r") as path_f:
        return json.load(path_f)


def _save_koji_cache(cache: Mapping[Any, Any], path: Path):
    # ensure we serialize an empty object at least
    cache = cache or {}
    with path.open("w") as path_f:
        cache_json = json.dumps(cache, indent=2) + os.linesep
        path_f.write(cache_json)


def _find_koji_package_name(
    package: str, arch: str, koji_session: koji.ClientSession
) -> Optional[str]:
    nvra = f"{package}.{arch}"
    logging.info("Checking Koji for build info for %s...", nvra)
    find_build = koji_session.getRPM(nvra)
    if find_build is None:
        return None

    logging.info("Build info: %s", json.dumps(find_build))
    logging.info("Checking Koji for package info for %s...", nvra)
    find_package = koji_session.getBuild(find_build["build_id"])
    if find_package is None:
        return None

    package_name = find_package["name"]

    # Corner case: seemingly no Koji or other API to determine this
    if package_name == "nspr":
        package_name = "nss"

    return package_name


async def _download_file(client: httpx.AsyncClient, url: str, output_path: Path, retries=5):
    for retry in range(retries):
        try:
            response = await client.get(url)
            response.raise_for_status()
            break
        except HTTPStatusError as hse:
            logging.exception("Download error:")
            if hse.response.status_code == codes.NOT_FOUND:
                raise hse
            logging.info("Retrying (%d/%d)...", retry + 1, retries)
    else:
        raise DownloadError(f"Unable to download {url} in {retries} retries")

    async with AIOFile(output_path, "wb") as output_f:
        content = await response.aread()
        await output_f.write(content)

    await response.aclose()
