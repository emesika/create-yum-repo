create-yum-repo
===============
`create-yum-repo` is a tool for composing the YUM repository containing the packages that comprise 
the Automotive SIG AutoSD.

### Usage
```
usage: create-yum-repo [-h] -l,--lockfile LOCKFILE [-o,--output-dir OUTPUT_DIR] [-r,--repo-url REPO_URLS [REPO_URLS ...]] [-k,--koji KOJI_INSTANCES [KOJI_INSTANCES ...]] {cs9,rhel9}

Create a YUM repo from constituent packages.

positional arguments:
  {cs9,rhel9}           The base distribution for the packages in the resulting YUM repository.

options:
  -h, --help            show this help message and exit
  -l,--lockfile LOCKFILE
                        Path to the lockfile.
  -o,--output-dir OUTPUT_DIR
                        The intermediate directory where RPMs will be downloaded and the YUM repo created.
  -r,--repo-url REPO_URLS [REPO_URLS ...]
                        A list of repo URLs in descending priority order.
  -k,--koji KOJI_INSTANCES [KOJI_INSTANCES ...]
                        A list of Koji instance URLs.
```

#### Examples

**Using the default output directory (`/var/lib/repos`), source repository URLs, and Koji URLs**
```shell
create-yum-repo cs9 -l path/to/lockfile.lock.json  
```

**With a custom output directory**
```shell
create-yum-repo cs9 -l path/to/lockfile.lock.json -o /tmp/create_yum_repo  
```

**With custom source repositories**  
`create-yum-repo` lets you run with custom source repo URLs like http://mirror.stream.centos.org/9-stream/BaseOS/x86_64/os/, 
and http://mirror.stream.centos.org/9-stream/AppStream/x86_64/os/. In order to avoid invoking the script with 
potentially dozens of URLs, you can use Python-style `{}` named format arguments in your URLs, and `create-yum-repo` 
will expand them out to a full set of URLs. See [accepted URL formatter names][_url_format_names] for additional details.

```shell
create-yum-repo cs9 \
    -l path/to/lockfile.lock.json \
    -r http://mirror.stream.centos.org/9-stream/{centos_repo}/{arch}/os/ \
    -r https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/{centos_repo}/{arch}/os/  
```

**With custom Koji instances**  
`create-yum-repo` will fall back to Koji when it is unable to find a package in the supplied source 
repositories. To configure which Koji instances are used in this case provide one or more Koji URLs 
in a pipe (`|`) delimited string like `<API endpoint>|<Packages endpoint>`.

```shell
create-yum-repo cs9 \
    -l path/to/lockfile.lock.json \
    -k "https://kojihub.stream.centos.org/kojihub/|https://kojihub.stream.centos.org/kojifiles/packages/" \
    -k "https://cbs.centos.org/kojihub/|https://cbs.centos.org/kojifiles/packages/"
```

### Accepted URL Format Args
The create-yum-repo script supports the following URL format arguments for any source repository URLs 
passed in using the `-r` or `--repo-url` flags:

| Name        | Usage         | Description                                |
|-------------|---------------|--------------------------------------------|
| centos_repo | {centos_repo} | This expands to BaseOS, AppStream, and CRB |
| arch        | {arch}        | This expands to x86_64, aarch64            |

> :pushpin: Your source repo URLs do not need to use all of these format args to be valid. They can use any 
subset of them and be expanded correctly. Any unexpected format args will cause an error.

### How does it work?
Using the [lockfile][_lockfile] definition from the Automotive SIG `sample-images` repository, a 
list of source repository URLs, and a list of Koji instance URLs as input `create-yum-repo` first builds 
a lookup mapping from package name-version-release-architecture (NVRA) strings to a priority queue of 
download URLs.

The download URLs are prioritized based on the order they're passed in to the program. For example, 
if `download_url_a` is passed before `download_url_b`, and a package is found in both `download_url_a`
and `download_url_b` then the program prefers downloading that package from `download_url_a`.

Once the lookup mapping is complete the program proceeds to download RPMs for all the packages to a 
staging directory. Once all packages are downloaded, a YUM repo is created from the staging directory 
structure using `createrepo_c`. 

Lastly, an index.html page is generated using `tree -H` in order to provide a simple index for the 
generated YUM repo to facilitate web browsing.

Contributing
------------
### Provisioning your Python environment for development
`create-yum-repo` has its dependencies broken up in to two files: `requirements.txt` for runtime dependencies, 
and `test_requirements.txt` for test-time dependencies. 

**Provision using pip**
```shell
pip install -r requirements.txt -r test_requirements.txt
```

**Provision using pipenv**  
If you use [pipenv][_pipenv] for managing virtual environments you can use the following to fully provision 
a new virtualenv. 
```shell
pipenv --python 3.9
pipenv install -r requirements.txt
pipenv install --dev -r test_requirements.txt
```

> :pushpin: If you do use `pipenv`, please refrain from committing your `Pipfile` or `Pipfile.lock`.

### pre-commit integration
With [**pre-commit**](https://pre-commit.com) you can test your code for small issues and run the appropriate 
linters locally. 

> :pushpin: To make use of this feature you will need to [install Pre-commit](#installing-pre-commit) on your local machine.

To enable this, simply run `pre-commit install` from the project directory at any stage after cloning and before committing. 
Now, every `git commit` run will be followed by the hooks defined in [.pre-commit-config.yaml](.pre-commit-config.yaml). Unless all tests have passed, the commit will be aborted.

**Running hooks**

The configured hooks will only run against the files that have been modified or added. To allow testing
on all files in the repository instead, you can use the following command:
```shell
pre-commit run --all-files
```

and to run individual hooks:
```shell
pre-commit run <hook_id>
```

> :pushpin: Some hooks have local dependencies (e.g. markdownlint requires RubyGems)

If you wish to perform pre-commit testing but want to skip any specific test/hook use SKIP on commit.
The SKIP environment variable is a comma separated list of hook ids as defined in [.pre-commit-config.yaml](.pre-commit-config.yaml)
```shell
SKIP=<hook_id>, <hook_id> git commit -m "foo"
```

#### Installing Pre-commit
To install the pre-commit package manager, run the respective command for your preferred package manager:
```shell
pip install pre-commit
```
```shell 
brew install pre-commit
```
```shell
conda install -c conda-forge pre-commit
```

### Linting 

#### Source Code
Our CI uses [pylint][_pylint] to lint our Python source files. You can run `pylint` locally to lint
the source code by installing `pylint` using your favorite Python package manager and then running:

```shell
# for source
pylint src/**/*.py

# for tests
pylint tests/**/*.py

# all at once - this is how our CI does it
find . -name '*.py' -exec pylint {} +
```

#### Containerfile
Our CI uses [hadolint][_hadolint] to lint our Containerfile. You can run `hadolint` locally to lint 
the `Containerfile` in the following ways:

**Using Docker**
```shell
docker run --rm -i ghcr.io/hadolint/hadolint < Containerfile
```

**Using Podman**
```shell
podman run --rm -i ghcr.io/hadolint/hadolint < Containerfile
```

> :pushpin: Our CI uses the .hadolint.yaml configuration file in the project root directory to set the  
> failure threshold to `error`. The usage examples here do not make use of this config file, but the 
> differences are transparent to the developer (exit status of `1` instead of `0` when `warning` is 
> emitted) and will not change the output.

If you'd prefer to install `hadolint` on your machine, follow [the installation instructions][_hadolint_install] 
from the `hadolint` project.

### Running tests
Contributors can run the project's test suite by invoking `pytest` from the project root directory. 

The project includes some sensible default pytest configuration in the `pytest.ini` file in the project 
root directory including:
* Adding `src/` to `PYTHONPATH` while running the tests
* Resolving test cases from Python files under the `tests/` directory
* Enabling statement coverage
* Enabling branch coverage
* Producing a JUnit test report
* Producing an HTML coverage report for humans
* Producing an XML coverage report for Cobertura 


### Building a container
Our CI uses [buildah][_buildah] to build container images from the Containerfile like so:
```shell
buildah bud -f Containerfile -t create-yum-repo:$TAG .
```
where `TAG` is `latest` when building a container image after merging to the default branch, or the 
name of the current branch when building a container image for merge requests.


[_lockfile]: https://gitlab.com/CentOS/automotive/sample-images/-/blob/main/package_list/cs9-image-manifest.lock.json
[_pipenv]: https://pipenv.pypa.io/en/latest/
[_hadolint]: https://github.com/hadolint/hadolint 
[_hadolint_install]: https://github.com/hadolint/hadolint#install 
[_pylint]: https://pylint.pycqa.org/en/latest/index.html 
[_url_format_names]: #accepted-url-format-args
[_buildah]: https://buildah.io/
[_gitlab_container_registry]: https://docs.gitlab.com/ee/user/packages/container_registry/
